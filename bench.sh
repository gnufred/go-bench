#!/bin/bash

go clean -testcache
go test "./..." -bench package -bench=.
