package main

import "testing"

// In this scenario I want to test which is faster between:
// if par.Ok == 0
// if par.Ok < 1
//
// Results varies, but usually using == is just a tiny bit slower
func BenchmarkOkEqZero(b *testing.B) {
	ok := 0
	for i := 0; i < b.N; i++ {
		if ok == 0 {
		}
	}
}

func BenchmarkOkSmallOne(b *testing.B) {
	ok := 0
	for i := 0; i < b.N; i++ {
		if ok < 1 {
		}
	}
}

func BenchmarkOkGreatOne(b *testing.B) {
	ok := 100
	for i := 0; i < b.N; i++ {
		if ok < 1 {
		}
	}
}

// In this scenario, I wanted to check if it was faster to use get matches by
// slicing NGINX log line based on AhoCorasick match position or by getting the
// information from the pattern slice variable.
// It's faster to cut the NGINX log, but I ended up not doing that anyway.

var botPatterns = []string{
	"APIs-Google",
	"AdsBot-Google",
	"AdsBot-Google-Mobile",
	"FeedFetcher-Google",
	"Google-AdWords",
	"Google-InspectionTool",
}

// BenchmarkGetStringFromSublice-8   	1000000000	         0.3137 ns/op
// BenchmarkGetStringFromSlice-8     	1000000000	         0.6072 ns/op
func BenchmarkGetStringFromSublice(b *testing.B) {
	got := "Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/W.X.Y.Z Mobile Safari/537.36 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)"
	nul := func(s string) {}

	for i := 0; i < b.N; i++ {
		bot := got[146:155]
		nul(bot)
	}
}

func BenchmarkGetStringFromSlice(b *testing.B) {
	nul := func(s string) {}

	for i := 0; i < b.N; i++ {
		bot := botPatterns[4]
		nul(bot)
	}
}
